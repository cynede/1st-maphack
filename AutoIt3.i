﻿/* AutoIt3.h 1: */
/* AutoIt3.h 2: */
/* AutoIt3.h 3: */
/* AutoIt3.h 4: */
/* AutoIt3.h 5: */
/* AutoIt3.h 6: */
/* AutoIt3.h 7: */
/* AutoIt3.h 8: */
/* AutoIt3.h 9: */
/* AutoIt3.h 10: */
/* AutoIt3.h 11: */
/* AutoIt3.h 12: */
/* AutoIt3.h 13: */
/* AutoIt3.h 14: */
/* AutoIt3.h 15: */
/* AutoIt3.h 16: */
/* AutoIt3.h 17: */
/* AutoIt3.h 18: */
/* AutoIt3.h 19: */
/* AutoIt3.h 20: */
/* AutoIt3.h 21: */
/* AutoIt3.h 22: */
/* AutoIt3.h 23: */
/* AutoIt3.h 24: */
/* AutoIt3.h 25: */
/* AutoIt3.h 26: */
/* AutoIt3.h 27: */
/* AutoIt3.h 28: */
/* AutoIt3.h 29: */
/* AutoIt3.h 30: */
/* AutoIt3.h 31: */
/* AutoIt3.h 32: */
/* AutoIt3.h 33: */
/* AutoIt3.h 34: */
/* AutoIt3.h 35: */
/* AutoIt3.h 36: */
/* AutoIt3.h 37: */
/* AutoIt3.h 38: */
/* AutoIt3.h 39: */
/* AutoIt3.h 40: */
/* AutoIt3.h 41: */
/* AutoIt3.h 42: */
/* AutoIt3.h 43: */
/* AutoIt3.h 44: */
/* AutoIt3.h 45: */
/* AutoIt3.h 46: */
/* AutoIt3.h 47: */
/* AutoIt3.h 48: */
/* AutoIt3.h 49: */
/* AutoIt3.h 50: */
/* AutoIt3.h 51: */
/* AutoIt3.h 52: */
/* AutoIt3.h 53: */ void WINAPI AU3_Init(void);
/* AutoIt3.h 54: */ long AU3_error(void);
/* AutoIt3.h 55: */
/* AutoIt3.h 56: */ long WINAPI AU3_AutoItSetOption(LPCWSTR szOption, long nValue);
/* AutoIt3.h 57: */
/* AutoIt3.h 58: */ void WINAPI AU3_BlockInput(long nFlag);
/* AutoIt3.h 59: */
/* AutoIt3.h 60: */ long WINAPI AU3_CDTray(LPCWSTR szDrive, LPCWSTR szAction);
/* AutoIt3.h 61: */ void WINAPI AU3_ClipGet(LPWSTR szClip, int nBufSize);
/* AutoIt3.h 62: */ void WINAPI AU3_ClipPut(LPCWSTR szClip);
/* AutoIt3.h 63: */ long WINAPI AU3_ControlClick(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPCWSTR szButton, long nNumClicks, long nX, long nY);
/* AutoIt3.h 64: */ void WINAPI AU3_ControlCommand(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPCWSTR szCommand, LPCWSTR szExtra, LPWSTR szResult, int nBufSize);
/* AutoIt3.h 65: */ void WINAPI AU3_ControlListView(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPCWSTR szCommand, LPCWSTR szExtra1, LPCWSTR szExtra2, LPWSTR szResult, int nBufSize);
/* AutoIt3.h 66: */ long WINAPI AU3_ControlDisable(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 67: */ long WINAPI AU3_ControlEnable(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 68: */ long WINAPI AU3_ControlFocus(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 69: */ void WINAPI AU3_ControlGetFocus(LPCWSTR szTitle, LPCWSTR szText, LPWSTR szControlWithFocus, int nBufSize);
/* AutoIt3.h 70: */ void WINAPI AU3_ControlGetHandle(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 71: */ long WINAPI AU3_ControlGetPosX(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 72: */ long WINAPI AU3_ControlGetPosY(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 73: */ long WINAPI AU3_ControlGetPosHeight(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 74: */ long WINAPI AU3_ControlGetPosWidth(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 75: */ void WINAPI AU3_ControlGetText(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPWSTR szControlText, int nBufSize);
/* AutoIt3.h 76: */ long WINAPI AU3_ControlHide(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 77: */ long WINAPI AU3_ControlMove(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, long nX, long nY, long nWidth, long nHeight);
/* AutoIt3.h 78: */ long WINAPI AU3_ControlSend(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPCWSTR szSendText, long nMode);
/* AutoIt3.h 79: */ long WINAPI AU3_ControlSetText(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPCWSTR szControlText);
/* AutoIt3.h 80: */ long WINAPI AU3_ControlShow(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl);
/* AutoIt3.h 81: */ void WINAPI AU3_ControlTreeView(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szControl, LPCWSTR szCommand, LPCWSTR szExtra1, LPCWSTR szExtra2, LPWSTR szResult, int nBufSize);
/* AutoIt3.h 82: */
/* AutoIt3.h 83: */ void WINAPI AU3_DriveMapAdd(LPCWSTR szDevice, LPCWSTR szShare, long nFlags, LPCWSTR szUser, LPCWSTR szPwd, LPWSTR szResult, int nBufSize);
/* AutoIt3.h 84: */ long WINAPI AU3_DriveMapDel(LPCWSTR szDevice);
/* AutoIt3.h 85: */ void WINAPI AU3_DriveMapGet(LPCWSTR szDevice, LPWSTR szMapping, int nBufSize);
/* AutoIt3.h 86: */
/* AutoIt3.h 87: */ long WINAPI AU3_IniDelete(LPCWSTR szFilename, LPCWSTR szSection, LPCWSTR szKey);
/* AutoIt3.h 88: */ void WINAPI AU3_IniRead(LPCWSTR szFilename, LPCWSTR szSection, LPCWSTR szKey, LPCWSTR szDefault, LPWSTR szValue, int nBufSize);
/* AutoIt3.h 89: */ long WINAPI AU3_IniWrite(LPCWSTR szFilename, LPCWSTR szSection, LPCWSTR szKey, LPCWSTR szValue);
/* AutoIt3.h 90: */ long WINAPI AU3_IsAdmin(void);
/* AutoIt3.h 91: */
/* AutoIt3.h 92: */ long WINAPI AU3_MouseClick(LPCWSTR szButton, long nX, long nY, long nClicks, long nSpeed);
/* AutoIt3.h 93: */ long WINAPI AU3_MouseClickDrag(LPCWSTR szButton, long nX1, long nY1, long nX2, long nY2, long nSpeed);
/* AutoIt3.h 94: */ void WINAPI AU3_MouseDown(LPCWSTR szButton);
/* AutoIt3.h 95: */ long WINAPI AU3_MouseGetCursor(void);
/* AutoIt3.h 96: */ long WINAPI AU3_MouseGetPosX(void);
/* AutoIt3.h 97: */ long WINAPI AU3_MouseGetPosY(void);
/* AutoIt3.h 98: */ long WINAPI AU3_MouseMove(long nX, long nY, long nSpeed);
/* AutoIt3.h 99: */ void WINAPI AU3_MouseUp(LPCWSTR szButton);
/* AutoIt3.h 100: */ void WINAPI AU3_MouseWheel(LPCWSTR szDirection, long nClicks);
/* AutoIt3.h 101: */
/* AutoIt3.h 102: */ long WINAPI AU3_Opt(LPCWSTR szOption, long nValue);
/* AutoIt3.h 103: */
/* AutoIt3.h 104: */ unsigned long WINAPI AU3_PixelChecksum(long nLeft, long nTop, long nRight, long nBottom, long nStep);
/* AutoIt3.h 105: */ long WINAPI AU3_PixelGetColor(long nX, long nY);
/* AutoIt3.h 106: */ void WINAPI AU3_PixelSearch(long nLeft, long nTop, long nRight, long nBottom, long nCol, long nVar, long nStep, LPPOINT pPointResult);
/* AutoIt3.h 107: */ long WINAPI AU3_ProcessClose(LPCWSTR szProcess);
/* AutoIt3.h 108: */ long WINAPI AU3_ProcessExists(LPCWSTR szProcess);
/* AutoIt3.h 109: */ long WINAPI AU3_ProcessSetPriority(LPCWSTR szProcess, long nPriority);
/* AutoIt3.h 110: */ long WINAPI AU3_ProcessWait(LPCWSTR szProcess, long nTimeout);
/* AutoIt3.h 111: */ long WINAPI AU3_ProcessWaitClose(LPCWSTR szProcess, long nTimeout);
/* AutoIt3.h 112: */ long WINAPI AU3_RegDeleteKey(LPCWSTR szKeyname);
/* AutoIt3.h 113: */ long WINAPI AU3_RegDeleteVal(LPCWSTR szKeyname, LPCWSTR szValuename);
/* AutoIt3.h 114: */ void WINAPI AU3_RegEnumKey(LPCWSTR szKeyname, long nInstance, LPWSTR szResult, int nBufSize);
/* AutoIt3.h 115: */ void WINAPI AU3_RegEnumVal(LPCWSTR szKeyname, long nInstance, LPWSTR szResult, int nBufSize);
/* AutoIt3.h 116: */ void WINAPI AU3_RegRead(LPCWSTR szKeyname, LPCWSTR szValuename, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 117: */ long WINAPI AU3_RegWrite(LPCWSTR szKeyname, LPCWSTR szValuename, LPCWSTR szType, LPCWSTR szValue);
/* AutoIt3.h 118: */ long WINAPI AU3_Run(LPCWSTR szRun, LPCWSTR szDir, long nShowFlags);
/* AutoIt3.h 119: */ long WINAPI AU3_RunAsSet(LPCWSTR szUser, LPCWSTR szDomain, LPCWSTR szPassword, int nOptions);
/* AutoIt3.h 120: */ long WINAPI AU3_RunWait(LPCWSTR szRun, LPCWSTR szDir, long nShowFlags);
/* AutoIt3.h 121: */
/* AutoIt3.h 122: */ void WINAPI AU3_Send(LPCWSTR szSendText, long nMode);
/* AutoIt3.h 123: */ void WINAPI AU3_SendA(LPCSTR szSendText, long nMode);
/* AutoIt3.h 124: */ long WINAPI AU3_Shutdown(long nFlags);
/* AutoIt3.h 125: */ void WINAPI AU3_Sleep(long nMilliseconds);
/* AutoIt3.h 126: */ void WINAPI AU3_StatusbarGetText(LPCWSTR szTitle, LPCWSTR szText, long nPart, LPWSTR szStatusText, int nBufSize);
/* AutoIt3.h 127: */
/* AutoIt3.h 128: */ void WINAPI AU3_ToolTip(LPCWSTR szTip, long nX, long nY);
/* AutoIt3.h 129: */
/* AutoIt3.h 130: */ void WINAPI AU3_WinActivate(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 131: */ long WINAPI AU3_WinActive(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 132: */ long WINAPI AU3_WinClose(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 133: */ long WINAPI AU3_WinExists(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 134: */ long WINAPI AU3_WinGetCaretPosX(void);
/* AutoIt3.h 135: */ long WINAPI AU3_WinGetCaretPosY(void);
/* AutoIt3.h 136: */ void WINAPI AU3_WinGetClassList(LPCWSTR szTitle, LPCWSTR szText, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 137: */ long WINAPI AU3_WinGetClientSizeHeight(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 138: */ long WINAPI AU3_WinGetClientSizeWidth(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 139: */ void WINAPI AU3_WinGetHandle(LPCWSTR szTitle, LPCWSTR szText, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 140: */ long WINAPI AU3_WinGetPosX(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 141: */ long WINAPI AU3_WinGetPosY(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 142: */ long WINAPI AU3_WinGetPosHeight(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 143: */ long WINAPI AU3_WinGetPosWidth(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 144: */ void WINAPI AU3_WinGetProcess(LPCWSTR szTitle, LPCWSTR szText, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 145: */ long WINAPI AU3_WinGetState(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 146: */ void WINAPI AU3_WinGetText(LPCWSTR szTitle, LPCWSTR szText, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 147: */ void WINAPI AU3_WinGetTitle(LPCWSTR szTitle, LPCWSTR szText, LPWSTR szRetText, int nBufSize);
/* AutoIt3.h 148: */ long WINAPI AU3_WinKill(LPCWSTR szTitle, LPCWSTR szText);
/* AutoIt3.h 149: */ long WINAPI AU3_WinMenuSelectItem(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szItem1, LPCWSTR szItem2, LPCWSTR szItem3, LPCWSTR szItem4, LPCWSTR szItem5, LPCWSTR szItem6, LPCWSTR szItem7, LPCWSTR szItem8);
/* AutoIt3.h 150: */ void WINAPI AU3_WinMinimizeAll();
/* AutoIt3.h 151: */ void WINAPI AU3_WinMinimizeAllUndo();
/* AutoIt3.h 152: */ long WINAPI AU3_WinMove(LPCWSTR szTitle, LPCWSTR szText, long nX, long nY, long nWidth, long nHeight);
/* AutoIt3.h 153: */ long WINAPI AU3_WinSetOnTop(LPCWSTR szTitle, LPCWSTR szText, long nFlag);
/* AutoIt3.h 154: */ long WINAPI AU3_WinSetState(LPCWSTR szTitle, LPCWSTR szText, long nFlags);
/* AutoIt3.h 155: */ long WINAPI AU3_WinSetTitle(LPCWSTR szTitle, LPCWSTR szText, LPCWSTR szNewTitle);
/* AutoIt3.h 156: */ long WINAPI AU3_WinSetTrans(LPCWSTR szTitle, LPCWSTR szText, long nTrans);
/* AutoIt3.h 157: */
/* AutoIt3.h 158: */ long WINAPI AU3_WinWait(LPCWSTR szTitle, LPCWSTR szText, long nTimeout);
/* AutoIt3.h 159: */ long WINAPI AU3_WinWaitA(LPCSTR szTitle, LPCSTR szText, long nTimeout);
/* AutoIt3.h 160: */ long WINAPI AU3_WinWaitActive(LPCWSTR szTitle, LPCWSTR szText, long nTimeout);
/* AutoIt3.h 161: */ long WINAPI AU3_WinWaitActiveA(LPCSTR szTitle, LPCSTR szText, long nTimeout);
/* AutoIt3.h 162: */ long WINAPI AU3_WinWaitClose(LPCWSTR szTitle, LPCWSTR szText, long nTimeout);
/* AutoIt3.h 163: */ long WINAPI AU3_WinWaitCloseA(LPCSTR szTitle, LPCSTR szText, long nTimeout);
/* AutoIt3.h 164: */ long WINAPI AU3_WinWaitNotActive(LPCWSTR szTitle, LPCWSTR szText, long nTimeout);
/* AutoIt3.h 165: */ long WINAPI AU3_WinWaitNotActiveA(LPCSTR szTitle, LPCSTR szText, long nTimeout);
/* AutoIt3.h 166: */
/* AutoIt3.h 167: */
/* AutoIt3.h 168: */
/* AutoIt3.h 169: */
/* AutoIt3.h 170: */
/* AutoIt3.h 171: */
/* AutoIt3.h 172: */