//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
HINSTANCE g_hinst;
//---------------------------------------------------------------------------
USEFORM("Unit1.cpp", Form1);
//---------------------------------------------------------------------------
WINAPI _tWinMain(HINSTANCE, HINSTANCE hInstance, LPTSTR, int)
{
  try{
    Application->Initialize();
    g_hinst = hInstance;
    Application->MainFormOnTaskBar = true;
    Application->Title             = "1st Hacks Free Maphack";
    Application->HelpFile          = "<3";
    Application->CreateForm(__classid(TForm1), &Form1);
    Application->Run();
  }
  catch (Exception& exception) {
    Application->ShowException(&exception);
  }
  catch (...) {
    try{
      throw Exception("");
    }
    catch (Exception& exception) {
      Application->ShowException(&exception);
    }
  }
  return 0;
}
