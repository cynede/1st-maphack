//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "sSkinManager.hpp"
#include "acNoteBook.hpp"
#include <ExtCtrls.hpp>
#include "sPageControl.hpp"
#include <ComCtrls.hpp>
#include "sButton.hpp"
#include "sCheckBox.hpp"
#include "sLabel.hpp"
#include "sMemo.hpp"
#include "sEdit.hpp"
#include <GIFImg.hpp>
#include "IdBaseComponent.hpp"
#include "IdComponent.hpp"
#include "IdExplicitTLSClientServerBase.hpp"
#include "IdMessage.hpp"
#include "IdMessageClient.hpp"
#include "IdSMTP.hpp"
#include "IdSMTPBase.hpp"
#include "IdTCPClient.hpp"
#include "IdTCPConnection.hpp"
#include <MPlayer.hpp>
#include <Menus.hpp>
#include <Graphics.hpp>

#define MYWM_NOTIFY    (WM_APP + 100)
#define IDC_MYICON     1006
extern HINSTANCE g_hinst;
LRESULT IconDrawItem(LPDRAWITEMSTRUCT lpdi);
//---------------------------------------------------------------------------
class TForm1 : public TForm {
  __published :               // IDE-managed Components
  TsSkinManager * sSkinManager1;
  TsPageControl *Note;
  TsTabSheet *   sTabSheet1;
  TsTabSheet *   Advanced;
  TsTabSheet *   Credits;
  TsCheckBox *   MapHack;
  TsButton *     Inject;
  TsLabel *      sLabel1;
  TsLabelFX *    sLabelFX2;
  TsLabelFX *    sLabelFX3;
  TsLabelFX *    sLabelFX4;
  TsLabelFX *    sLabelFX5;
  TsMemo *       LogX;
  TsCheckBox *   Clickable;
  TsCheckBox *   Illu;
  TEdit *        Zoom;
  TsLabelFX *    sLabelFX1;
  TsCheckBox *   foggy;
  TsCheckBox *   pingy;
  TsCheckBox *   inviz;
  TsCheckBox *   dota;
  TsCheckBox *   items;
  TsCheckBox *   colored;
  TsCheckBox *   skills;
  TsCheckBox *   cooldown;
  TsCheckBox *   CIN;
  TsCheckBox *   SA;
  TsMemo *       sMemo1;
  TsButton *     sButton1;
  TsButton *     sButton2;
  TsCheckBox *   Trade;
  TsCheckBox *   hico;
  TsCheckBox *   sCheckBox1;
  TsCheckBox *   sCheckBox2;
  TsCheckBox *   sCheckBox3;
  TsEdit *       sEdit1;
  TsEdit *       sEdit2;
  TsLabelFX *    sLabelFX6;
  TsCheckBox *   Donate;
  TsLabelFX *    sLabelFX7;
  TsLabelFX *    sLabelFX8;
  TImage *       Image13;
  TIdMessage *   IdMessage1;
  TIdSMTP *      IdSMTP1;
  TMediaPlayer * omg;
  TPopupMenu *   PopupMenu2;
  TMenuItem *    Properties1;
  TMenuItem *    ToggleState1;
  TMenuItem *    Shutdown1;
  TRadioButton * RadioButton1;
  TRadioButton * RadioButton2;
  TImage *       Image1;
  TImage *       Image2;
  TsWebLabel *   sWebLabel1;
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall InjectClick(TObject *Sender);
  void __fastcall sButton1Click(TObject *Sender);
  void __fastcall SAClick(TObject *Sender);
  void __fastcall sButton2Click(TObject *Sender);
  void __fastcall omgNotify(TObject *Sender);
  void __fastcall Shutdown1Click(TObject *Sender);
  void __fastcall Properties1Click(TObject *Sender);
  void __fastcall ToggleState1Click(TObject *Sender);
  void __fastcall FormDestroy(TObject *Sender);
  void __fastcall FormClose(TObject *Sender, TCloseAction& Action);
private: // User declarations
  void __fastcall DrawItem(TMessage& Msg);
  void __fastcall MyNotify(TMessage& Msg);
  void __fastcall ToggleState(void);
  bool __fastcall TrayMessage(DWORD dwMessage);
  PWSTR __fastcall TipText(void);
  HICON __fastcall IconHandle(void);
public:  // User declarations
  __fastcall TForm1(TComponent *Owner);
  BEGIN_MESSAGE_MAP MESSAGE_HANDLER(WM_DRAWITEM, TMessage, DrawItem) MESSAGE_HANDLER(MYWM_NOTIFY, TMessage, MyNotify) END_MESSAGE_MAP(TForm)
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
